using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using NavMeshBuilder = UnityEngine.AI.NavMeshBuilder;

public enum Type
{
    Floor,
    Wall,
    Corner,
    DoorExit,
    DoorEnter,
    Furniture,
}

public enum Direction
{
    None,
    South,
    North,
    East,
    West
}

public class TileInfos
{
    public Type TileType;

    public GameObject wallObject;
    public GameObject floorObject;
    public GameObject furnitureObject = null;
}


public class Generator : MonoBehaviour
{
    [HideInInspector]
    public List<Map> Maps = new List<Map>();

    public NavMeshSurface navMeshsurface;

    public GameObject[] Meubles;

    public Floodfill floodFill;

    [HideInInspector]
    public Map CurrentMap;

    public GameObject ArrowDirection;

    public Map mapType;

    public float TileScale;

    public int dimensionX;
    public int dimensionY;

    public GameObject wallObject;
    public GameObject floorObject;

    public GameObject doorObject;

    public event EventHandler OnRoomChange;

    private Vector3 NavMeshSize;

    private NavMeshData navMeshData;
    private List<NavMeshBuildSource> Sources = new List<NavMeshBuildSource>();

    private int level = 5;

    private void Awake()
    {
        NavMeshSize = new Vector3(dimensionX * 2, dimensionY * 2, dimensionY * 2);

    }

    void Start()
    {
        floodFill = GetComponent<Floodfill>();
        ArrowDirection.active = false;
        ArrowDirection.SetActive(false);
        CreateMap(null);
    }

    private void BuildNavMesh(bool Asybc)
    {
        Bounds navMeshBounds = new Bounds(CurrentMap.position, NavMeshSize);

        List<NavMeshBuildMarkup> markups = new List<NavMeshBuildMarkup>();

        List<NavMeshModifier> modifiers;
        if (navMeshsurface.collectObjects == CollectObjects.Children)
        {
            modifiers = new List<NavMeshModifier>(navMeshsurface.GetComponentsInChildren<NavMeshModifier>());
        }
        else
        {
            modifiers = NavMeshModifier.activeModifiers;
        }

        for (int i = 0; i < modifiers.Count; i++)
        {
            if (((navMeshsurface.layerMask & (1 << modifiers[i].gameObject.layer)) == 1) && modifiers[i].AffectsAgentType(navMeshsurface.agentTypeID))
            {
                markups.Add(new NavMeshBuildMarkup()
                {
                    root = modifiers[i].transform,
                    overrideArea = modifiers[i].overrideArea,
                    area = modifiers[i].area,
                    ignoreFromBuild = modifiers[i].ignoreFromBuild,
                });
            }
        }

        if (navMeshsurface.collectObjects == CollectObjects.Children)
        {
            NavMeshBuilder.CollectSources(navMeshsurface.transform, navMeshsurface.layerMask, navMeshsurface.useGeometry, navMeshsurface.defaultArea, markups, Sources);
        }
        else
        {
            NavMeshBuilder.CollectSources(navMeshBounds, navMeshsurface.layerMask, navMeshsurface.useGeometry, navMeshsurface.defaultArea, markups, Sources);
        }

        Sources.RemoveAll(source => source.component.gameObject.GetComponent<NavMeshAgent>() != null);

        NavMeshBuilder.UpdateNavMeshData(navMeshData, navMeshsurface.GetBuildSettings(), Sources, new Bounds(CurrentMap.position, NavMeshSize));
        NavMesh.AddNavMeshData(navMeshData);
        navMeshsurface.BuildNavMesh();
    }

    public void CreateMap(Map previous)
    {
        if (previous != null)
        {
            Destroy(previous.MapTiles[(int)previous.exitDoorPosition.x, (int)previous.exitDoorPosition.y].wallObject);
        }

        Map temp = new Map(dimensionX, dimensionY);
        CurrentMap = temp;

        Maps.Add(temp);


        if (previous != null)
        {
            temp.position = NextMapPosition(previous, previous.doorFacing);
        }
        else
        {
            temp.position = new Vector3(0, 0, 0);
        }
        temp.trackingPosition = Vector3Int.FloorToInt(temp.position);


        RenderWalls(temp, previous);
        CreateDoor(temp, 0);
        temp.floorObject = Instantiate(floorObject, new Vector3(temp.position.x - 1f + (dimensionX / 2), 0f, temp.position.y + dimensionY / 2), Quaternion.identity);
        temp.floorObject.transform.localScale = Vector3.one * (dimensionX / 8f);

        if (previous != null)
        {
            SpawnExtraWalls(temp);
        }

        SpawnColorable(level);

        if (level < 10)
            level++;

        if (previous != null)
        {
            ClearEntrances(temp);
        }
        StartCoroutine(ArrowPointingCoroutine(previous));

        navMeshData = new NavMeshData();
        NavMesh.AddNavMeshData(navMeshData);
        BuildNavMesh(false);


        OnRoomChange?.Invoke(temp, null);
    }

    public IEnumerator ArrowPointingCoroutine(Map temp)
    {
        if (temp != null)
        {
            ArrowDirection.active = true;

            ArrowDirection.GetComponent<Image>().color = Color.yellow;
            ArrowDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, 0f);
            switch (temp.doorFacing)
            {
                case Direction.South:
                    ArrowDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, 0f);
                    break;
                case Direction.East:
                    ArrowDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, -90f);
                    break;
                case Direction.West:
                    ArrowDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, 90f);
                    break;
                case Direction.North:
                    ArrowDirection.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, 0f, 180f);
                    break;
            }

            for (int i = 0; i < 10f; i++)
            {
                Color start = i % 2 == 0 ? Color.yellow : Color.red;
                Color end = i % 2 == 0 ? Color.red : Color.yellow;

                ArrowDirection.transform.localScale = Vector3.one;

                LeanTween.value(ArrowDirection, start, end, 1f)
                .setLoopPingPong()
                .setEasePunch()
                .setOnUpdateColor((Color val) =>
                {
                    ArrowDirection.GetComponent<Image>().color = val;
                });
                LeanTween.scale(ArrowDirection, new Vector3(1.5f, 1.5f, 1.5f), 1f).setEasePunch();
                yield return new WaitForSeconds(0.5f);
            }
        }

        ArrowDirection.active = false;
    }


    public void RenderWalls(Map map, Map previous)
    {
        map.MapTiles = new TileInfos[map.width, map.height];

        for (int i = 0; i < map.height; i++)
        {
            for (int j = 0; j < map.width; j++)
            {
                map.MapTiles[j, i] = new TileInfos();
                if (IsBorder(map, j, i))
                {
                    if ((previous != null && CanSpawnWall(j, i, previous)) || previous == null)
                        RenderBorders(map, j, i, 2f);
                    if (IsCorner(map, j, i))
                        map.MapTiles[j, i].TileType = Type.Corner;
                    else
                        map.MapTiles[j, i].TileType = Type.Wall;
                }
                else
                {
                    map.MapTiles[j, i].TileType = Type.Floor;
                }
            }
        }
    }

    public Vector3 RealPosition(Map map, int j, int i, float elevation = 0)
    {
        return new Vector3(map.position.x + (j * TileScale), elevation, map.position.y + (i * TileScale));
    }


    public void RenderBorders(Map map, int j, int i, float elevation = 0)
    {
        map.MapTiles[j, i].wallObject = Instantiate(wallObject, RealPosition(map, j, i, elevation), Quaternion.identity);

        map.Walls.Add(map.MapTiles[j, i].wallObject);

    }

    public bool CanSpawnWall(int j, int i, Map previous)
    {
        if (previous.exitDoorPosition.x == 0 && j == dimensionX - 1)
        {
            if (previous.exitDoorPosition.y == i)
                return false;
        }

        if (previous.exitDoorPosition.x == dimensionX - 1 && j == 0)
        {
            if (previous.exitDoorPosition.y == i)
                return false;
        }

        if (previous.exitDoorPosition.y == 0 && i == dimensionY - 1)
        {
            if (previous.exitDoorPosition.x == j)
                return false;
        }

        if (previous.exitDoorPosition.y == dimensionY - 1 && i == 0)
        {
            if (previous.exitDoorPosition.x == j)
                return false;
        }

        return true;
    }

    public bool IsBorder(Map map, int x, int y)
    {
        if (x == 0 || y == 0 || x == map.width - 1 || y == map.height - 1)
            return true;
        return false;
    }
    public bool IsCorner(Map map, int x, int y)
    {
        if (x == 0 && y == 0)
            return true;
        if (x == map.width - 1 && y == map.height - 1)
            return true;
        if (x == 0 && y == map.height - 1)
            return true;
        if (x == map.width - 1 && y == 0)
            return true;
        return false;
    }

    public void ClearEntrances(Map map)
    {
        for (int i = 1; i < map.height - 1; i++)
        {
            for (int j = 1; j < map.height - 1; j++)
            {
                if (i == 1 || i == map.height - 2 || j == 1 || j == map.width - 2)
                {
                    if (map.MapTiles[j, i].wallObject != null)
                    {
                        Destroy(map.MapTiles[j, i].wallObject);
                    }
                }
            }
        }
    }

    public bool CreateDoor(Map map, int iterations)
    {
        if (iterations > 20)
            return false;

        Direction direction = (Direction)UnityEngine.Random.Range(1, 5);

        if (DirectionIsAccepted(map, direction))
        {
            map.doorFacing = direction;
            switch (direction)
            {
                case Direction.North:
                    Debug.Log("North door created");
                    int i = UnityEngine.Random.Range(1, map.width - 1);

                    map.MapTiles[i, 0].TileType = Type.DoorExit;
                    map.exitDoorPosition = new Vector2(i, 0);

                    Destroy(map.MapTiles[i, 0].wallObject);

                    if (doorObject != null)
                        map.MapTiles[i, 0].wallObject = Instantiate(doorObject, RealPosition(map, i, 0), Quaternion.Euler(-90f, 0f, 0f));
                    break;
                case Direction.South:
                    Debug.Log("South door created");
                    int j = UnityEngine.Random.Range(1, map.width - 1);

                    map.MapTiles[j, map.height - 1].TileType = Type.DoorExit;

                    map.exitDoorPosition = new Vector2(j, map.height - 1);

                    Destroy(map.MapTiles[j, map.height - 1].wallObject);
                    if (doorObject != null)
                        map.MapTiles[j, map.height - 1].wallObject = Instantiate(doorObject, RealPosition(map, j, map.height - 1), Quaternion.Euler(-90f, 0f, 0f));

                    break;
                case Direction.East:
                    Debug.Log("East door created");
                    int k = UnityEngine.Random.Range(1, map.height - 1);

                    map.MapTiles[map.width - 1, k].TileType = Type.DoorExit;

                    map.exitDoorPosition = new Vector2(map.width - 1, k);

                    Destroy(map.MapTiles[map.width - 1, k].wallObject);
                    if (doorObject != null)
                        map.MapTiles[map.width - 1, k].wallObject = Instantiate(doorObject, RealPosition(map, map.width - 1, k), Quaternion.Euler(-90f, -90f, 0f));

                    break;
                case Direction.West:
                    Debug.Log("West door created");

                    int l = UnityEngine.Random.Range(1, map.height - 1);

                    map.MapTiles[0, l].TileType = Type.DoorExit;

                    map.exitDoorPosition = new Vector2(0, l);

                    Destroy(map.MapTiles[0, l].wallObject);
                    if (doorObject != null)
                        map.MapTiles[0, l].wallObject = Instantiate(doorObject, RealPosition(map, 0, l), Quaternion.Euler(-90f, -90f, 0f));

                    break;
                default:
                    Debug.Log("No door created");
                    break;
            }
            return true;
        }
        else
        {
            CreateDoor(map, iterations++);
        }
        return false;
    }

    public bool DirectionIsAccepted(Map map, Direction direction)
    {
        Vector3Int nextMapPos = NextMapPosition(map, direction);

        Map foundMap = Maps.Find(map => map.trackingPosition == nextMapPos);

        if (foundMap == null)
            return true;

        return false;
    }

    public Vector3Int NextMapPosition(Map map, Direction direction)
    {
        if (map == null)
            return Vector3Int.zero;
        if (direction == Direction.North)
            return map.trackingPosition + new Vector3Int(0, -dimensionY, 0);
        if (direction == Direction.South)
            return map.trackingPosition + new Vector3Int(0, dimensionY, 0);
        if (direction == Direction.East)
            return map.trackingPosition + new Vector3Int(dimensionX, 0, 0);
        if (direction == Direction.West)
            return map.trackingPosition + new Vector3Int(-dimensionX, 0, 0);
        return Vector3Int.zero;
    }


    public void SpawnColorable(int level)
    {

        for (int i = 0; i < level; i++)
        {
            int x = UnityEngine.Random.Range(3, CurrentMap.width - 2);
            int y = UnityEngine.Random.Range(3, CurrentMap.height - 2);

            if (CurrentMap.MapTiles[x, y].TileType == Type.Furniture || CurrentMap.MapTiles[x, y].TileType == Type.Wall)
            {
                i--;
                continue;
            }

            CurrentMap.MapTiles[x, y].furnitureObject = Instantiate(Meubles[UnityEngine.Random.Range(0, Meubles.Length)], RealPosition(CurrentMap, x, y), Quaternion.Euler(-90f, UnityEngine.Random.Range(0, 180), 0f));
            CurrentMap.Meubles.Add(CurrentMap.MapTiles[x, y].furnitureObject);
            CurrentMap.MapTiles[x, y].TileType = Type.Furniture;
        }
    }

    public void SpawnExtraWalls(Map map)
    {
        if (UnityEngine.Random.Range(0, 100) <= 76)
        {
            int x = UnityEngine.Random.Range(3, CurrentMap.width - 2);
            int y = UnityEngine.Random.Range(3, CurrentMap.height - 2);

            floodFill.FloodFillAlgo(map, x, y, true);
        }

        if (UnityEngine.Random.Range(0, 100) <= 67)
        {
            int x = UnityEngine.Random.Range(3, CurrentMap.width - 2);
            int y = UnityEngine.Random.Range(3, CurrentMap.height - 2);

            floodFill.FloodFillAlgo(map, x, y, true);
        }


        if (UnityEngine.Random.Range(0, 100) <= 65)
        {
            int range = UnityEngine.Random.Range(6, 12);

            int numb = UnityEngine.Random.Range(4, 10);
            int numb2 = UnityEngine.Random.Range(4, 10);

            if (UnityEngine.Random.Range(0, 100) <= 50)
            {
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[dimensionX - numb, numb2 + i].wallObject = Instantiate(wallObject, RealPosition(map, dimensionX - numb, numb2 + i, 0.5f), Quaternion.identity);
                    map.MapTiles[dimensionX - numb, numb2 + i].TileType = Type.Wall;
                }
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[numb2 + i, numb].wallObject = Instantiate(wallObject, RealPosition(map, numb2 + i, numb, 0.5f), Quaternion.identity);
                    map.MapTiles[numb2 + i, numb].TileType = Type.Wall;
                }
            }
            else
            {
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[map.width - numb - i, numb2].wallObject = Instantiate(wallObject, RealPosition(map, map.width - numb - i, numb2, 0.5f), Quaternion.identity);
                    map.MapTiles[map.width - numb - i, numb2].TileType = Type.Wall;
                }
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[numb2 + i, map.height - numb - i].wallObject = Instantiate(wallObject, RealPosition(map, numb2 + i, map.height - numb - i, 0.5f), Quaternion.identity);
                    map.MapTiles[numb2 + i, map.height - numb - i].TileType = Type.Wall;
                }
            }
        }
        if (UnityEngine.Random.Range(0, 100) <= 50)
        {
            int x = UnityEngine.Random.Range(3, CurrentMap.width - 2);
            int y = UnityEngine.Random.Range(3, CurrentMap.height - 2);

            floodFill.FloodFillAlgo(map, x, y);
        }

        if (UnityEngine.Random.Range(0, 100) <= 45)
        {
            int x = UnityEngine.Random.Range(3, CurrentMap.width - 2);
            int y = UnityEngine.Random.Range(3, CurrentMap.height - 2);

            floodFill.FloodFillAlgo(map, x, y);
        }

        if (UnityEngine.Random.Range(0, 100) <= 60)
        {
            int range = UnityEngine.Random.Range(6, 12);

            int numb = UnityEngine.Random.Range(4, 10);
            int numb2 = UnityEngine.Random.Range(4, 10);

            if (UnityEngine.Random.Range(0, 100) < 50)
            {
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[map.width - numb - i, numb2].wallObject = Instantiate(wallObject, RealPosition(map, map.width - numb - i, numb2, 0.5f), Quaternion.identity);
                    map.MapTiles[map.width - numb - i, numb2].TileType = Type.Wall;
                }
                for (int i = 0; i < range; i++)
                {
                    map.MapTiles[map.width - numb, numb2 + i].wallObject = Instantiate(wallObject, RealPosition(map, map.width - numb, numb2 + i, 0.5f), Quaternion.identity);
                    map.MapTiles[map.width - numb, numb2 + i].TileType = Type.Wall;
                }
            }
            else
            {
                for (int i = 0; i < range; i++)
                {
                    if (map.width - numb >= 0)
                    {
                        map.MapTiles[map.width - numb2, map.height - numb - i].wallObject = Instantiate(wallObject, RealPosition(map, map.width - numb2, map.height - numb - i, 0.5f), Quaternion.identity);
                        map.MapTiles[map.width - numb2, map.height - numb - i].TileType = Type.Wall;
                    }
                }
                for (int i = 0; i < range; i++)
                {
                    if (map.height - numb >= 0)
                    {
                        map.MapTiles[map.width - numb2 - i, map.height - numb].wallObject = Instantiate(wallObject, RealPosition(map, map.width - numb2 - i, map.height - numb, 0.5f), Quaternion.identity);
                        map.MapTiles[map.width - numb2 - i, map.height - numb].TileType = Type.Wall;
                    }
                }
            }
        }

    }

}
