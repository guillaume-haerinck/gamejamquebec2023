using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Floodfill : MonoBehaviour
{

    public int MaxTilecount = 6;
    public int MaxTileReach = 12;

    private List<Vector2Int> _Positions;

    [HideInInspector]
    public Vector2Int LeftInt;
    [HideInInspector]
    public Vector2Int RightInt;
    [HideInInspector]
    public Vector2Int UpInt;
    [HideInInspector]
    public Vector2Int DownInt;

    private int _TileCount = 0;
    public Generator generator;
    private bool[,] _CheckedMap;

    Floodfill()
    {
        _Positions = new List<Vector2Int>();
        LeftInt = new Vector2Int(-1, 0);
        RightInt = new Vector2Int(1, 0);
        UpInt = new Vector2Int(0, 1);
        DownInt = new Vector2Int(0, -1);

        _Positions.Add(LeftInt);
        _Positions.Add(RightInt);
        _Positions.Add(UpInt);
        _Positions.Add(DownInt);
    }


    private void ShuffleConditions()
    {
        for (int i = _Positions.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i + 1);
            var temp = _Positions[i];
            _Positions[i] = _Positions[j];
            _Positions[j] = temp;
        }
    }


    public Map FloodFillAlgo(Map map, int startX, int startY, bool half = false)
    {
        if (map == null)
            return null;

        _CheckedMap = new bool[map.width, map.height];

        map.MapTiles[startX, startY].TileType = Type.Wall;
        map.MapTiles[startX, startY].wallObject = Instantiate(generator.wallObject, generator.RealPosition(map, startX, startY), Quaternion.identity);

        _TileCount = 1; // Starts tile count at one (already have first position)
        FloodFillRecursive(map, startX, startY, 0, half); // Start Recursive Flood fill at Position. 

        return map;
    }


    private void FloodFillRecursive(Map map, int x, int y, int n, bool half = false)
    {
        if (n >= MaxTileReach || (half && n >= MaxTileReach / 2))
            return;


        ShuffleConditions();
        List<Vector2Int> list = new List<Vector2Int>(_Positions);

        foreach (Vector2Int position in list)
        {
            if (x + position.x < 0 || x + position.x > map.width - 1 || y + position.y < 0 || y + position.y > map.height - 1)
                continue;

            if (MaxTilecount > 1 && _TileCount > MaxTilecount)
                break;

            if (!_CheckedMap[x + position.x, y + position.y] && LevelChecks(map.MapTiles[x + position.x, y + position.y], x + position.x, y + position.y, map)) // TILE IS INCLUDED TO REGION
            {
                _TileCount++; // Increase count of Tiles in Region

                _CheckedMap[x + position.x, y + position.y] = true; // Position has been checked, No more occurence for this position.

                map.MapTiles[x + position.x, y + position.y].TileType = Type.Wall;
                map.MapTiles[x + position.x, y + position.y].wallObject = Instantiate(generator.wallObject, generator.RealPosition(map, x + position.x, y + position.y, 1f), Quaternion.identity);

                FloodFillRecursive(map, x + position.x, y + position.y, n++);
            }
        }
    }

    private bool LevelChecks(TileInfos tileInfos, int x, int y, Map map)
    {
        if (tileInfos.TileType == Type.Corner || tileInfos.TileType == Type.Wall || tileInfos.TileType == Type.Furniture)
            return false;

        if (x < 3 || y < 3) return false;
        if (x > map.width - 2 || y > map.height - 2) return false;

        return true;
    }
}
