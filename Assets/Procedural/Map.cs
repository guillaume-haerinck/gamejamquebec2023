using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Map : ScriptableObject
{
    public Map(int x, int y)
    {
        width = x; height = y;
    }

    public int width;
    public int height;

    [HideInInspector]
    public TileInfos[,] MapTiles;

    public List<GameObject> Meubles {get; private set;} = new List<GameObject> ();

    public List<GameObject> Walls = new List<GameObject> ();

    [HideInInspector]
    public Vector3 position;
    [HideInInspector]
    public Vector3Int trackingPosition;

    [HideInInspector]
    public Vector2 exitDoorPosition;
    [HideInInspector]
    public Direction doorFacing;

    public int clutter;
    [HideInInspector]
    public GameObject floorObject;
}
