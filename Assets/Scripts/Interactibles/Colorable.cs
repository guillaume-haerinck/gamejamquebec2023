using UnityEngine;

public class Colorable : MonoBehaviour
{
    Renderer rendererComp;
    public Color CurrentColor { get; private set; }

    public Vector3 originalScale;

    void Start()
    {
        originalScale = transform.localScale;
        rendererComp = GetComponent<Renderer>();
    }

    public void ShotWithColor(Color color)
    {
        CurrentColor = color;
        rendererComp.material.color = color;
        GameManager.Instance.InvokeOnColorableColored();

        gameObject.transform.localScale = originalScale;
        LeanTween.scale(gameObject, new Vector3(1.5f, 1.5f, 1.5f), 0.3f).setEasePunch().setOnComplete(() => gameObject.transform.localScale = originalScale);
        Instantiate(GameManager.Instance.Confetti, transform.position, Quaternion.identity);
    }
}
