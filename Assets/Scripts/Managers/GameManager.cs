using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public Generator generator;

    [HideInInspector]
    public GameObject Player;
    [HideInInspector]
    public GameObject Camera;
    [HideInInspector]
    public PlayerInput playerInput;

    private EnemyManager _enemyManager;
    public Transform[] SpawnPoint;

    public GameObject Confetti;

    public List<Color> AvailableColors { get; private set; } = new();

    public uint CurrentColorIndex { get; private set; } = 0;
    public uint RoomCountFinished { get; private set; } = 0;

    /*
    public static event EventHandler onActiveColorChanged;
    public static event EventHandler onNewColorAdded;
    public static event EventHandler onColorableColored;
    public static event EventHandler onNewRoomStarted;
    */
    public event EventHandler onActiveColorChanged;
    public event EventHandler onNewColorAdded;
    public event EventHandler onColorableColored;
    public event EventHandler onNewRoomStarted;

    public List<GameObject> CurrentRoomColorables => generator.CurrentMap.Meubles;
    public Dictionary<Color, int> ColorablesToDoToUnlockNextRoom { get; private set; } = new();
    public Dictionary<Color, int> ColorablesRemainingToUnlockNextRoom { get; private set; } = new();

    private bool _startOfGame = true;

    public void InvokeOnColorableColored()
    {
        if (VictoryConditionMetForCurrentRoom())
        {
            RoomCountFinished++;
            generator.CreateMap(generator.CurrentMap);
        }
        else
        {
            onColorableColored?.Invoke(null, null);
        }
    }

    public void Awake()
    {
        Instance = this;

        Player = GameObject.Find("Player");
        Camera = GameObject.Find("Main Camera");
        playerInput = GetComponent<PlayerInput>();
        _enemyManager = GetComponent<EnemyManager>();

        if (Player != null && generator != null)
        {
            Debug.Log("Player put in pos");
            Player.transform.position = new Vector3(generator.dimensionX / 2, 0.03f, generator.dimensionY / 2);
            Camera.transform.position += new Vector3(generator.dimensionX / 2, 0.03f, generator.dimensionY / 2);
        }
        //generator.OnRoomChange += OnRoomTrigger;
        AvailableColors = new List<Color>
        {
            Color.red
        };
        onNewColorAdded?.Invoke(null, null);
        onActiveColorChanged?.Invoke(null, null);
        generator.OnRoomChange += OnRoomTrigger;
    }

    private void Start()
    {
        InvokeRepeating("UseNextColor", 5.0f, 5.0f);
    }

    private void StartNewRoom()
    {
        AddAvailableColorIfNeeded();
        ColorablesToDoToUnlockNextRoom.Clear();
        foreach (Color color in AvailableColors)
        {
            ColorablesToDoToUnlockNextRoom.Add(color, 0);
        }

        int count = Mathf.Max(1, (int)(CurrentRoomColorables.Count * 0.7f));
        for (int i = 0; i < count; i++)
        {
            ColorablesToDoToUnlockNextRoom[GetRandomColorInAvailable()]++;
        }

        ColorablesRemainingToUnlockNextRoom = new(ColorablesToDoToUnlockNextRoom);
        onNewRoomStarted?.Invoke(null, null);
    }

    private bool VictoryConditionMetForCurrentRoom()
    {
        bool result = true;
        foreach (KeyValuePair<Color, int> goal in ColorablesToDoToUnlockNextRoom)
        {
            int currentCountForColor = 0;
            foreach (GameObject colorableGO in CurrentRoomColorables)
            {
                if (colorableGO.TryGetComponent(out Colorable colorable))
                {
                    if (colorable.CurrentColor == goal.Key)
                    {
                        currentCountForColor++;
                    }
                }
            }

            ColorablesRemainingToUnlockNextRoom[goal.Key] = Mathf.Max(0, goal.Value - currentCountForColor);
            if (currentCountForColor < goal.Value)
                result = false;
        }

        return result;
    }

    private void AddAvailableColorIfNeeded()
    {
        switch (RoomCountFinished)
        {
            case 1:
                AvailableColors.Add(Color.blue);
                break;

            case 3:
                AvailableColors.Add(Color.magenta);
                break;

            case 5:
                AvailableColors.Add(Color.green);
                break;

            default:
                return;
        }

        onNewColorAdded?.Invoke(null, null);
    }

    private Color GetRandomColorInAvailable()
    {
        int index = UnityEngine.Random.Range(0, AvailableColors.Count);
        return AvailableColors[index];
    }

    private void OnRoomTrigger(object sender, EventArgs e)
    {
        StartNewRoom();
        Map map = (Map)sender;

        SpawnPoint[0].transform.position = new Vector3(map.position.x + (map.width * 0.9f), 0.02f, map.position.y + (map.height * 0.9f));
        SpawnPoint[1].transform.position = new Vector3(map.position.x + (map.width * 0.9f), 0.02f, map.position.y + 2f);
        SpawnPoint[2].transform.position = new Vector3(map.position.x + 2f, 0.02f, map.position.y + (map.height * 0.9f));
        SpawnPoint[3].transform.position = new Vector3(map.position.x + 2f, 0.02f, map.position.y + 2f);

        if (_startOfGame)
        {
            _startOfGame = false;
            _enemyManager.Activate();
        }

    }

    private void UseNextColor()
    {
        if (AvailableColors.Count <= 1)
            return;

        if (CurrentColorIndex + 1 < AvailableColors.Count)
        {
            CurrentColorIndex++;
        }
        else
        {
            CurrentColorIndex = 0;
        }
        onActiveColorChanged?.Invoke(null, null);
    }

}
