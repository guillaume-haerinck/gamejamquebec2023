using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour
{

    public AudioMixerSnapshot paused;
    public AudioMixerSnapshot unpaused;
    public GameObject menuObjFirst;
    public PlayerHealth playerHealth;
    public TimerManager timerManager;
    Canvas canvas;
    void Start()
    {
        canvas = GetComponent<Canvas>();
        EventSystem.current.SetSelectedGameObject(menuObjFirst);
    }

    void Update()
    {
        if (PlayerController.Instance.playerControls.Controls.Pause.triggered && (playerHealth.CurrentHealth >= 0 || timerManager.timerLeft > 0))
        {
            canvas.enabled = !canvas.enabled;
            Pause();
        }
    }

    public void Pause()
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        Lowpass();

    }

    void Lowpass()
    {
        if (Time.timeScale == 0)
        {
            paused.TransitionTo(.01f);
        }

        else
        {
            unpaused.TransitionTo(.01f);
        }
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }
}
