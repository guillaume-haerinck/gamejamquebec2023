using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public TimerManager timerManager;
    public GameObject highScoreCanvas;

    public Text gameOverRestartText;


    void Update()
    {
        if (playerHealth.CurrentHealth <= 0 || timerManager.timerLeft == 0)
        {
            timerManager.timerLeft = 0;
            highScoreCanvas.SetActive(true);
            GetComponent<Canvas>().enabled = false;
            if (PlayerController.Instance.playerControls.Controls.RestartGame.triggered && PlayerController.Instance.isGamePad)
            {
                if(PlayerController.Instance.isGamePad)
                    gameOverRestartText.text = "PRESS SELECT TO RESTART GAME";
                else
                    gameOverRestartText.text = "PRESS R TO RESTART GAME";

                SceneManager.UnloadSceneAsync(1);
                SceneManager.LoadScene(0);
            }
        }
    }

    public static void ReturnToMenu()
    {
        SceneManager.UnloadSceneAsync(1);
        SceneManager.LoadScene(0);
    }


}
