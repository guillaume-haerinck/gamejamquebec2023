using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[HideInInspector]
public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject[] EnemyStyles;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    public List<GameObject> enemies;

    [HideInInspector]
    public Generator generator;
    [HideInInspector]
    public int Level = 0;

    private void Awake()
    {
        generator = GameObject.Find("Generator").GetComponent<Generator>();
        generator.OnRoomChange += HandleLevelIncrease;
        Deactivate();
    }

    private void HandleLevelIncrease(object sender, System.EventArgs e)
    {
        Level++;

        Debug.Log("Up to Level : " + Level);

        Deactivate();
        Activate();

        if (spawnTime > 1f)
        {
            spawnTime -= 0.2f;
        }
    }

    void Spawn()
    {
        if (playerHealth.CurrentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        int chooseIndex = Level > 3 ? EnemyStyles.Length : Level;

        int enemyIndex = UnityEngine.Random.Range(0, chooseIndex);

        var go = Instantiate(EnemyStyles[enemyIndex], spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        enemies.Add(go);

        EnemyHealth enemyHealth = go.GetComponent<EnemyHealth>();
        enemyHealth.scoreValue = 2 + (uint)enemyIndex;

        if (Level > 12)
        {
            go.GetComponent<NavMeshAgent>().speed = 5.4f + enemyIndex * 0.4f;
        }
        else
        {
            go.GetComponent<NavMeshAgent>().speed = 3 + (0.2f * Level) + enemyIndex * 0.2f;
        }
    }

    public void Deactivate()
    {
        gameObject.SetActive(gameObject);
    }

    public void Activate()
    {
        gameObject.SetActive(true);

        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

}
