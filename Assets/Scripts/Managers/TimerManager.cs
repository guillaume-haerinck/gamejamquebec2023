using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{
    bool timerOn;
    public float TimePerLevel = 60f;
    [HideInInspector]
    public float timerLeft;
    public Text timerTxt;

    private bool TimePunchActivated = false;

    private Coroutine coroutine = null; 

    void Start()
    {
        timerLeft = TimePerLevel;
        timerOn = true;
        GameObject.Find("Generator").GetComponent<Generator>().OnRoomChange += ResetTimer;
    }

    void LateUpdate()
    {
        if (timerOn)
        {
            if (timerLeft > 0)
            {
                timerLeft -= Time.deltaTime;
                UpdateTimer(timerLeft);
            }
            else
            {
                timerLeft = 0;
                timerOn = false;
            }

            if (timerLeft > 30.1f)
            {
                gameObject.GetComponent<Text>().color = Color.white;
            }
            else if (timerLeft < 11f && !TimePunchActivated && timerLeft % 1f < 0.1f && timerLeft % 5f > -0.1f)
            {
                TimePunchActivated = true;

                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                }
                coroutine = StartCoroutine(TimerColorCoroutine());
            }
            else if (timerLeft < 30.1f &&  !TimePunchActivated && timerLeft % 5f < 0.1f && timerLeft % 5f > -0.1f)
            {
                TimePunchActivated = true;

                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                }

                coroutine = StartCoroutine(TimerColorCoroutine());
            }
        }
    }

    IEnumerator TimerColorCoroutine()
    {
        for (int i = 0; i < 1f; i++)
        {
            Color start = i % 2 == 0 ? Color.yellow : Color.red;
            Color end = i % 2 == 0 ? Color.red : Color.yellow;

            gameObject.transform.localScale = Vector3.one;

            LeanTween.value(gameObject, start, end, 1f)
            .setLoopPingPong()
            .setEasePunch()
            .setOnUpdateColor((Color val) =>
            {
                GetComponent<Text>().color = val;
            });
            LeanTween.scale(gameObject, new Vector3(1.5f, 1.5f, 1.5f), 1f).setEasePunch();
            yield return new WaitForSeconds(0.9f);
            GetComponent<Text>().color = Color.white;
        }

        TimePunchActivated = false;
    }

    void UpdateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);
        if(timerTxt)
            timerTxt.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void ResetTimer(object sender, EventArgs e)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine); 
        }

        timerLeft = TimePerLevel;
        TimePunchActivated = false;
    }
}
