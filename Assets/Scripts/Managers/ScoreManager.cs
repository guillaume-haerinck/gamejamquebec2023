using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static uint score = 0;
    public static int highScore = 0;

    public Text scoreText;
    public Text highScoreText;

    public TMP_InputField inputField;


    private void Awake()
    {
        inputField.onValidateInput = (string text, int index, char addedChar) => {
            return OnValidChar("0123456789qwertyuiopasdfghjklzxcvbnm QWERTYUIOPASDFGHJKLZXCVBNM", addedChar);
        };
        inputField.characterLimit = 24;

    }

    void OnEnable()
    {
        inputField.onValidateInput = (string text, int index, char addedChar) => {
            return OnValidChar("0123456789qwertyuiopasdfghjklzxcvbnm QWERTYUIOPASDFGHJKLZXCVBNM", addedChar);
        };

        inputField.onValidateInput = (string text, int index, char addedChar) => {
            return OnValidChar("0123456789", addedChar);
        };

        score = LiveScoreManager.score;

        highScore = PlayerPrefs.GetInt("Highscore0", 0);
        scoreText.text = (score).ToString() + " POINTS";
        highScoreText.text = "Best Score: " + highScore.ToString();

        /*
        if (highScore < score)
            PlayerPrefs.SetInt("Highscore", (int)score);
        */
    }

    public void SubmitScore()
    {
        for (int i = 0; i < 11 ; i++)
        {
            highScore = PlayerPrefs.GetInt("Highscore" + i.ToString(), 0);
            if (highScore < score)
            {
                int tempScore = PlayerPrefs.GetInt("Highscore" + i.ToString(), 0);
                string tempName = PlayerPrefs.GetString("HighscoreName" + i.ToString(), "");

                PlayerPrefs.SetInt("Highscore" + i, (int)score);
                PlayerPrefs.SetString("HighscoreName" + i, inputField.text);

                for (int j = i + 1; j < 12; j++)
                {
                    int tempScore2 = PlayerPrefs.GetInt("Highscore" + (j).ToString(), 0);
                    string tempName2 = PlayerPrefs.GetString("HighscoreName" + (j).ToString(), "");

                    PlayerPrefs.SetInt("Highscore" + j, tempScore);
                    PlayerPrefs.SetString("HighscoreName" + j, tempName);

                    tempScore = tempScore2;
                    tempName = tempName2;
                }
                break;
            }
        }

        GameOverManager.ReturnToMenu();
    }

    private char OnValidChar(string validchar, char addedChar)
    {
        if (validchar.IndexOf(addedChar) == -1)
        {
            //valid
            return addedChar;
        }
        else
        {
            //invalid
            return '\0';
        }
    }


}
