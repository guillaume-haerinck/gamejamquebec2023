using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    public Renderer shootingArea;

    public float timer;
    ParticleSystem gunParticles;
    AudioSource gunAudio;
    public float effectsDisplayTime = 0.2f;

    HashSet<EnemyHealth> enemiesInRange = new();
    public ParticleSystemRenderer particleSystemMaterial;

    private GameManager gameManager;

    void Awake()
    {
        gunParticles = GetComponentInChildren<ParticleSystem>();
        gunAudio = GetComponent<AudioSource>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.onActiveColorChanged += HandleActiveColorChanged;
    }

    private void Start()
    {
        //GameManager.Instance.onActiveColorChanged += HandleActiveColorChanged;
    }

    public void Shoot()
    {
        timer = 0f;

        gunAudio.Play();

        gunParticles.Stop();
        gunParticles.Play();

        foreach (EnemyHealth enemyHealth in enemiesInRange)
        {
            if (enemyHealth == null || !IsInPlayerShootFOV(enemyHealth.transform))
            {
                continue;
            }

            enemyHealth.TakeDamage(damagePerShot, enemyHealth.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyHealth health))
        {
            health.EnableDecal(true);
            enemiesInRange.Add(health);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyHealth health))
        {
            health.EnableDecal(false);
            enemiesInRange.Remove(health);
        }
    }

    // Collider is a box, need to trim results to have enemies in the cone in front of player
    private bool IsInPlayerShootFOV(Transform inTransform)
    {
        // GH_TODO
        return true;
    }

    private void HandleActiveColorChanged(object sender, EventArgs e)
    {
        particleSystemMaterial.material.SetColor("_BaseColor", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
        foreach (ParticleSystemRenderer particleSystemRenderer in particleSystemMaterial.GetComponentsInChildren<Renderer>())
        {
            particleSystemRenderer.material.SetColor("_BaseColor", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
        }
    }
}
