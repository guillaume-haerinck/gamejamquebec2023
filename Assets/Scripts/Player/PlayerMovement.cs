using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{

    public float speed = 6f;

    public ParticleSystem dashParticle;
    public AudioClip dashClip;

    float controllerDeadZone = 0.1f;
    float gamepadRotateSmoothing = 1000f;

    Animator animator;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    float delayBeforeInvinsible = 0.2f;
    float invinsibleDuration = 0.5f;

    PlayerHealth playerHealth;
    private PlayerShooting playerShooting;
    AudioSource playerAudio;

    [Header("Dashing")]
    bool dashing = false;
    bool wantToDash = false;
    float dashForce = 15.0f;
    float dashDuration = 0.25f;


    [Header("Cooldown")]
    public float dashCd = 1.0f;
    private float dashCdTimer;

    void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
        floorMask = LayerMask.GetMask("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
        playerHealth = GetComponent<PlayerHealth>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (PlayerController.Instance.playerControls.Controls.Dash.triggered && !dashing)
        {
            wantToDash = true;
        }
    }

    void FixedUpdate()
    {
        Move();
        Turning();
        Animating();

        if (wantToDash)
        {
            wantToDash = false;
            Dash();
        }

        if (dashCdTimer > 0)
            dashCdTimer -= Time.deltaTime;

        playerShooting.timer += Time.deltaTime;

        if (PlayerController.Instance.canShoot && playerShooting.timer >= playerShooting.timeBetweenBullets && Time.timeScale != 0)
        {
            playerShooting.Shoot();
        }
    }

    void Move()
    {
        Vector3 move = new Vector3(PlayerController.Instance.movement.x, 0, PlayerController.Instance.movement.y);
        move = move.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + move);
    }

    void Turning()
    {
        if (PlayerController.Instance.isGamePad)
        {
            if (Mathf.Abs(PlayerController.Instance.aim.x) > controllerDeadZone || Mathf.Abs(PlayerController.Instance.aim.y) > controllerDeadZone)
            {
                Vector3 playerDirection = Vector3.right * PlayerController.Instance.aim.x + Vector3.forward * PlayerController.Instance.aim.y;
                if (playerDirection.sqrMagnitude > 0.0f)
                {
                    Quaternion newRot = Quaternion.LookRotation(playerDirection, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, newRot, gamepadRotateSmoothing * Time.deltaTime);
                }
            }
        }
        else
        {
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit floorHit;
            if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
            {
                Vector3 playerToMouse = floorHit.point - transform.position;
                playerToMouse.y = 0;

                var rotation = Quaternion.LookRotation(playerToMouse);
                playerRigidbody.MoveRotation(rotation);
            }
        }
    }

    void Animating()
    {
        bool walking = PlayerController.Instance.movement.x != 0f || PlayerController.Instance.movement.y != 0f;
        animator.SetBool("IsWalking", walking);
    }

    void Dash()
    {
        if (dashCdTimer > 0) return;
        else dashCdTimer = dashCd;

        dashing = true;
        playerAudio.clip = dashClip;
        playerAudio.Play();
        dashParticle.Play();

        Vector3 direction = new Vector3(PlayerController.Instance.movement.x, 0, PlayerController.Instance.movement.y);
        Vector3 forceToApply = direction.normalized * dashForce;
        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.AddForce(forceToApply, ForceMode.Impulse);
        Invoke(nameof(ResetDash), dashDuration);
    }

    private void ResetDash()
    {
        playerRigidbody.velocity = Vector3.zero;
        dashing = false;
        dashParticle.Stop();
    }
}
