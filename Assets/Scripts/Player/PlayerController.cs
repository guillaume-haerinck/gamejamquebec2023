using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public static PlayerController Instance { get; private set; }

    [HideInInspector]
    public Vector3 movement;
    [HideInInspector]
    public Vector3 aim;
    [HideInInspector]
    public bool isGamePad;
    [HideInInspector]
    public bool canShoot;
    public PlayerControls playerControls;


    private void OnEnable()
    {
        playerControls.Enable();
        canShoot = false;
        playerControls.Controls.Fire.started += context => canShoot = true;
        playerControls.Controls.Fire.performed += context => canShoot = true;
        playerControls.Controls.Fire.canceled += context => canShoot = false;
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        playerControls = new PlayerControls();
    }

    void FixedUpdate()
    {
        HandleInput();
    }

    void HandleInput()
    {
        movement = playerControls.Controls.Movement.ReadValue<Vector2>();
        aim = playerControls.Controls.Aim.ReadValue<Vector2>();
    }

    public void OnDeviceChange()
    {
        if (GameManager.Instance && GameManager.Instance.playerInput)
            isGamePad = GameManager.Instance.playerInput.currentControlScheme.Equals("GamepadController") ? true : false;
    }
}
