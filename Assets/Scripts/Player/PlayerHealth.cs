using JetBrains.Annotations;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    float invinsibleAmt;

    public Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    bool isDead;
    bool damaged;
    int currentHealth;

    public GameObject camera;

    public float shakeDuration = 13f;
    public float shakeIntensity = 0.28f;

    public bool canShake = true;


    public int CurrentHealth { get { return currentHealth; } }

    void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<PlayerMovement>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        currentHealth = startingHealth;

        GameObject.Find("Generator").GetComponent<Generator>().OnRoomChange += HandleRoomChange;
    }

    private void HandleRoomChange(object sender, System.EventArgs e)
    {
        if (!TakeDamage(-20))
        {
            StartCoroutine(RegainHealthOnRoomChange());
        }
    }

    IEnumerator RegainHealthOnRoomChange()
    {
        yield return new WaitForSeconds(shakeDuration);
        if (!TakeDamage(-20))
        {
            StartCoroutine(RegainHealthOnRoomChange());
        }
    }

    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
        if (invinsibleAmt > 0)
        {
            invinsibleAmt -= Time.deltaTime;
        }
    }


    public bool TakeDamage(int amount)
    {
        if (!canShake)
            return false;

        if (invinsibleAmt <= 0)
        {
            damaged = true;
            currentHealth -= amount;
            if (currentHealth > 100)
                currentHealth = 100;
        }


        healthSlider.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);

        healthSlider.value = currentHealth;
        LeanTween.value(healthSlider.gameObject, healthSlider.GetComponentInChildren<Image>().color, Color.red, 1f)
                    .setLoopPingPong()
            .setEasePunch()
            .setOnUpdateColor((Color val) =>
            {
                healthSlider.gameObject.GetComponentInChildren<Image>().color = val;
            });

        LeanTween.scale(healthSlider.gameObject, new Vector3(3f, 3f, 3f), 1f).setEasePunch();

        StartCoroutine(ShakeCamera());

        playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        return true;
    }

    public IEnumerator ShakeCamera()
    {
        canShake = false;
        LeanTween.move(camera, camera.transform.position + Random.insideUnitSphere * shakeIntensity, shakeDuration)
            .setEaseShake();
        yield return new WaitForSeconds(shakeDuration);
        canShake = true;
    }


    public void Invinsible(float delayBeforeInvinsible, float invinsibleDuration)
    {
        if (delayBeforeInvinsible > 0)
        {
            StartCoroutine(StartInvinsible(delayBeforeInvinsible, invinsibleDuration));
        }
        else
        {
            invinsibleAmt = invinsibleDuration;
        }
    }

    IEnumerator StartInvinsible(float delayBeforeInvinsible, float invinsibleDuration)
    {
        yield return new WaitForSeconds(delayBeforeInvinsible);
        invinsibleAmt = invinsibleDuration;
    }

    void Death()
    {
        isDead = true;

        if (anim != null)
            anim.SetTrigger("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
}
