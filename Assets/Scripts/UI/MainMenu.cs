using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject controlsPanel;

    public GameObject mainMenuFirst;

    private void Start()
    {
        EventSystem.current.SetSelectedGameObject(mainMenuFirst);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

    public void CanvasClick()
    {
        if (controlsPanel.activeInHierarchy)
            controlsPanel.SetActive(false);
    }

    public void ShowControls()
    {
        controlsPanel.SetActive(!controlsPanel.activeInHierarchy);
    }

}
