using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenuScoreManager : MonoBehaviour
{
    public GameObject First;
    public Text FirstName;
    public Text FirstScore;

    public GameObject Second;
    public Text SecondName;
    public Text SecondScore;

    public GameObject Third;
    public Text ThirdName;
    public Text ThirdScore;

    public RectTransform OtherScoresPanel;
    public GameObject OtherScores;



    // Start is called before the first frame update
    void Start()
    {
        First.SetActive(false);
        Second.SetActive(false);
        Third.SetActive(false);

        int tempScore = PlayerPrefs.GetInt("Highscore" + 0.ToString(), 0);
        string tempName = PlayerPrefs.GetString("HighscoreName" + 0.ToString(), "");
        if (tempScore != 0)
        {
            First.SetActive(true);
            FirstName.text = tempName;
            FirstScore.text = tempScore.ToString();
        }

        tempScore = PlayerPrefs.GetInt("Highscore" + 1.ToString(), 0);
        tempName = PlayerPrefs.GetString("HighscoreName" + 1.ToString(), "");
        if (tempScore != 0)
        {
            Second.SetActive(true);
            SecondName.text = tempName;
            SecondScore.text = tempScore.ToString();
        }

        tempScore = PlayerPrefs.GetInt("Highscore" + 2.ToString(), 0);
        tempName = PlayerPrefs.GetString("HighscoreName" + 2.ToString(), "");
        if (tempScore != 0)
        {
            Third.SetActive(true);
            ThirdName.text = tempName;
            ThirdScore.text = tempScore.ToString();
        }

        for (int i = 3; i < 11; i++)
        {
            tempScore = PlayerPrefs.GetInt("Highscore" + i.ToString(), 0);
            tempName = PlayerPrefs.GetString("HighscoreName" + i.ToString(), "");

            if (tempScore != 0)
            {
                GameObject newScore = Instantiate(OtherScores, Vector3.zero, Quaternion.identity);

                RectTransform rectTransform = newScore.GetComponent<RectTransform>();
                rectTransform.SetParent(OtherScoresPanel);

                newScore.transform.Find("Rank").GetComponent<Text>().text = (i + 1).ToString() + '.';
                newScore.transform.Find("Name").GetComponent<Text>().text = tempName;
                newScore.transform.Find("Score").GetComponent<Text>().text = tempScore.ToString();
            }
            else
                break;
        }
    }
}
