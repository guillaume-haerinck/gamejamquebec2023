using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LiveScoreManager : MonoBehaviour
{

    public static uint enemyScore = 0;
    public static uint score = 0;

    public Text textAmount;

    public float AnimationTime = 0.3f;

    private uint oldScore = 0;
    private bool transition = false;


    private void Start()
    {
        enemyScore = 0;
        score = 0;
        textAmount.text = score.ToString();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        score = (GameManager.Instance.RoomCountFinished * (100 + 10 * GameManager.Instance.RoomCountFinished)) + enemyScore;

        if (score != oldScore && !transition)
        {
            transition = true;
            StartCoroutine(ScoreTransitionCoroutine());
            oldScore = score;
        }
        if (!transition)
        {
            textAmount.transform.localScale = Vector3.one;
            textAmount.color = Color.white;
        }

    }

    IEnumerator ScoreTransitionCoroutine()
    {
        StartFluctuationAnimation();
        yield return new WaitForSeconds(AnimationTime);
        transition = false;
    }

    void StartFluctuationAnimation()
    {
        // Use LeanTween to animate the value
        LeanTween.value(gameObject, oldScore, score, AnimationTime - 0.01f)
            .setOnUpdate(UpdateTextValue)
            .setEase(LeanTweenType.easeInOutQuad);
        Vector3 punch = new Vector3(1.5f, 1.5f, 1.5f);

        if (score - oldScore > 50)
        {
            punch = new Vector3(3f, 3f, 3f);
            LeanTween.value(gameObject, Color.yellow, Color.green, AnimationTime * 2 - 0.01f)
            .setOnUpdate(UpdateTextColor)
            .setEase(LeanTweenType.easeInOutQuad);
            LeanTween.scale(gameObject, punch, AnimationTime * 2 - 0.01f).setEasePunch();
        }
        else
            LeanTween.scale(gameObject, punch, AnimationTime - 0.01f).setEasePunch();
    }

    void UpdateTextColor(Color newColor)
    {
        // Update the text color as the animation progresses
        textAmount.color = newColor;
    }

    void UpdateTextValue(float newValue)
    {
        // Update the text value as the animation progresses
        textAmount.text = Mathf.RoundToInt(newValue).ToString();
    }
}
