using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class CarouselView : MonoBehaviour
{

    readonly List<RectTransform> images = new List<RectTransform>();
    public RectTransform view_window;

    private float image_width;
    private float screenPosition;

    public float image_gap = 30;
    private int m_currentIndex;

    public int CurrentIndex { get { return m_currentIndex; } }

    readonly List<GameObject> m_WheelColorComponents = new List<GameObject>();

    private GameManager gameManager;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.onNewRoomStarted += UpdateColorCounts;
        gameManager.onColorableColored += UpdateColorCounts;
        gameManager.onActiveColorChanged += HandleActiveColorChanged;
        gameManager.onNewColorAdded += HandleNewColorAdded;

    }

    private void Start()
    {
        
    }

    private void UpdateColorCounts(object sender, EventArgs e)
    {
        foreach (var component in m_WheelColorComponents)
        {
            Color compColor = component.GetComponent<Image>().color;
            component.GetComponentInChildren<Text>().text = GameManager.Instance.ColorablesRemainingToUnlockNextRoom[compColor].ToString();
        }
    }

    private void HandleNewColorAdded(object sender, EventArgs e)
    {
        GameObject wheelColorComponent = Resources.Load<GameObject>("Prefabs/WheelColorComponent");
        GameObject instantiateTo = GameObject.Find("ProfileScroll/CartoonWindow");

        foreach (GameObject oldWheel in m_WheelColorComponents)
        {
            Destroy(oldWheel);
        }

        m_WheelColorComponents.Clear();
        images.Clear();

        foreach (Color color in GameManager.Instance.AvailableColors)
        {
            if (wheelColorComponent)
            {
                var instatiatedWheelColorComponent = Instantiate(wheelColorComponent, instantiateTo.transform);
                m_WheelColorComponents.Add(instatiatedWheelColorComponent);
                images.Add(instatiatedWheelColorComponent.GetComponent<RectTransform>());
                instatiatedWheelColorComponent.GetComponent<Image>().color = color;

                if (GameManager.Instance.ColorablesRemainingToUnlockNextRoom.TryGetValue(color, out int value))
                {
                    instatiatedWheelColorComponent.GetComponentInChildren<Text>().text = value.ToString();
                }
            }
        }

        if(view_window.rect!= null)
            image_width = view_window.rect.width;
        for (int i = 1; i < images.Count; i++)
        {
            images[i].anchoredPosition = new Vector2(((image_width + image_gap) * i), 0);
        }
    }

    private void HandleActiveColorChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < images.Count; i++)
        {
            if (i == GameManager.Instance.CurrentColorIndex)
            {
                images[i].gameObject.GetComponent<RectTransform>().localScale = images[i].gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1.2f);
            }
            else
            {
                images[i].gameObject.GetComponent<RectTransform>().localScale = images[i].gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
            }
        }
    }
}
