using System;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public float sinkSpeed = 2.5f;
    public uint scoreValue = 2;
    public float damageSphereRadius = 4.0f;
    public AudioClip deathClip;

    public Animator anim;
    public GameObject decal;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    bool isDead;
    int currentHealth;
    SkinnedMeshRenderer skinnedMeshRenderer;

    public int CurrentHealth { get { return currentHealth; } }

    void Awake()
    {
        enemyAudio = GetComponent<AudioSource>();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        skinnedMeshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        currentHealth = startingHealth;
    }

    public void EnableDecal(bool enable)
    {
        decal.SetActive(enable);
    }

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        if (isDead) return;

        enemyAudio.Play();

        currentHealth -= amount;

        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        var colliders = Physics.OverlapSphere(transform.position, damageSphereRadius);
        foreach (var collider in colliders)
        {
            if (collider.gameObject.TryGetComponent(out Colorable colorable))
            {
                colorable.ShotWithColor(GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
            }
        }

        if (currentHealth <= 0)
        {
            Death();
        }
    }

    private void Start()
    {
        if (skinnedMeshRenderer)
            skinnedMeshRenderer.material.SetColor("_BaseColor", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
        if (decal)
            decal.GetComponent<Renderer>().material.SetColor("_Color", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);

        GameManager.Instance.onActiveColorChanged += HandleActiveColorChanged;
    }


    void Death()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger("Dead");

        enemyAudio.clip = deathClip;

        LiveScoreManager.enemyScore += scoreValue;

        enemyAudio.Play();
        StartSinking();
    }

    public void StartSinking()
    {
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        Destroy(gameObject, 2f);
    }

    void HandleActiveColorChanged(object sender, EventArgs e)
    {
        if (skinnedMeshRenderer)
            skinnedMeshRenderer.material.SetColor("_BaseColor", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
        if (decal)
            decal.GetComponent<Renderer>().material.SetColor("_Color", GameManager.Instance.AvailableColors[(int)GameManager.Instance.CurrentColorIndex]);
    }
}
